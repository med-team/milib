/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import java.util.Arrays;

public final class CompactIntArrayList {
    private CompactIntArrayList() {
    }

    public static final int DEFAULT_CAPACITY = 4;

    public static int[] addRecord(int[] list, int record) {
        if (list == null)
            // creating
            list = new int[DEFAULT_CAPACITY];
        else if (list[0] == list.length - 1)
            // growing
            list = Arrays.copyOf(list, (list.length * 3) / 2 + 1);
        list[0]++;
        list[list[0]] = record;
        return list;
    }

    public static long[] addRecord(long[] list, long record) {
        if (list == null)
            // creating
            list = new long[DEFAULT_CAPACITY];
        else if (list[0] == list.length - 1)
            // growing
            list = Arrays.copyOf(list, (list.length * 3) / 2 + 1);
        list[0]++;
        list[(int) list[0]] = record;
        return list;
    }
}
