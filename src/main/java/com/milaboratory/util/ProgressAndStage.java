/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import java.util.concurrent.atomic.AtomicReference;

public final class ProgressAndStage implements CanReportProgressAndStage {
    private volatile String stage;
    private volatile double progress = Double.NaN;
    private volatile boolean finished;

    private final AtomicReference<CanReportProgressAndStage> delegatePS = new AtomicReference<>();

    private final AtomicReference<CanReportProgress> delegateP = new AtomicReference<>();

    public ProgressAndStage(String stage) {
        this.stage = stage;
    }

    public ProgressAndStage(String stage, double progress) {
        this.stage = stage;
        this.progress = progress;
    }

    private CanReportProgressAndStage getDelegatePS() {
        CanReportProgressAndStage delegatePS = this.delegatePS.get();
        if (delegatePS != null && delegatePS.isFinished()) {
            this.stage = delegatePS.getStage();
            this.progress = delegatePS.getProgress();
            return this.delegatePS.compareAndSet(delegatePS, null)
                    ? null
                    : getDelegatePS();
        }
        return delegatePS;
    }

    private CanReportProgress getDelegateP() {
        CanReportProgress delegateP = this.delegateP.get();
        if (delegateP != null && delegateP.isFinished()) {
            this.progress = delegateP.getProgress();
            return this.delegateP.compareAndSet(delegateP, null)
                    ? null
                    : getDelegatePS();
        }
        return delegateP;
    }

    @Override
    public String getStage() {
        CanReportProgressAndStage delegatePS = getDelegatePS();
        return delegatePS != null
                ? delegatePS.getStage()
                : stage;
    }

    @Override
    public double getProgress() {
        CanReportProgressAndStage delegatePS = getDelegatePS();
        CanReportProgress delegateP = getDelegateP();
        return delegatePS != null
                ? delegatePS.getProgress()
                : delegateP != null
                ? delegateP.getProgress()
                : progress;
    }

    @Override
    public boolean isFinished() {
        CanReportProgressAndStage delegatePS = getDelegatePS();
        CanReportProgress delegateP = getDelegateP();
        return delegatePS == null && delegateP == null && finished;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    @Deprecated
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public void finish() {
        unDelegate();
        finished = true;
    }

    public void delegate(CanReportProgressAndStage psDelegate) {
        unDelegate();
        this.delegatePS.set(psDelegate);
    }

    public void delegate(CanReportProgress pDelegate) {
        unDelegate();
        this.delegateP.set(pDelegate);
    }

    public void delegate(String stage, CanReportProgress pDelegate) {
        unDelegate();
        this.stage = stage;
        this.delegateP.set(pDelegate);
    }

    public void unDelegate() {
        this.delegatePS.set(null);
        this.delegateP.set(null);
    }

}
