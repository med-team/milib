/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util.sorting;

import java.util.*;
import java.util.function.Function;

public final class SortingUtil {
    private SortingUtil() {
    }

    public static <T> Comparator<T> combine(Comparator<? super T>[] comparators) {
        return SortingUtil.<T>combine(Arrays.asList(comparators));
    }

    public static <T> Comparator<T> combine(List<? extends Comparator<? super T>> comparators) {
        if (comparators.size() == 0)
            throw new IllegalArgumentException();
        Comparator<T> comparator = (Comparator) comparators.get(0);
        for (int i = 1; i < comparators.size(); i++)
            comparator = comparator.thenComparing(comparators.get(i));
        return comparator;
    }

    static <T, E> SortingProperty<T> wrapped(SortingProperty<? super E> elementProperty, Function<T, E> extractor) {
        return new SortingProperty<T>() {
            @Override
            public Object get(T obj) {
                return elementProperty.get(extractor.apply(obj));
            }

            @Override
            public int compare(T o1, T o2) {
                return elementProperty.compare(extractor.apply(o1), extractor.apply(o2));
            }
        };
    }

    /**
     * Returns new list, where objects are hierarchically grouped together according to the key sequence.
     * Original is not modified by the method.
     */
    public static <T> List<T> hGroup(List<T> objects, List<? extends Function<T, Object>> keys) {
        HashMap<Object, List<T>> previous = new HashMap<>();
        previous.put(new Object(), objects);
        for (int i = keys.size() - 1; i >= 0; i--) {
            Function<T, Object> key = keys.get(i);
            HashMap<Object, List<T>> current = new HashMap<>(Math.max(16, previous.size()));
            for (List<T> values : previous.values())
                for (T value : values) {
                    Object keyValue = key.apply(value);
                    List<T> bucket = current.computeIfAbsent(keyValue, __ -> new ArrayList<>());
                    bucket.add(value);
                }
            previous = current;
        }

        ArrayList<T> result = new ArrayList<>(objects.size());
        for (List<T> values : previous.values())
            result.addAll(values);
        return result;
    }
}
