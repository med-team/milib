/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util.sorting;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.util.List;
import java.util.function.ToIntFunction;

public final class HashUtils {
    private HashUtils() {
    }

    @SuppressWarnings("UnstableApiUsage")
    public static <T> ToIntFunction<T> hashFunctionFor(List<? extends Property<? super T, ?>> properties) {
        return obj -> {
            Hasher hasher = Hashing.murmur3_32_fixed().newHasher();
            for (Property<? super T, ?> prop : properties) {
                hasher.putInt(prop.get(obj).hashCode());
            }
            return hasher.hash().hashCode();
        };
    }
}
