/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/** Lexicographical list comparator. */
public final class ListComparator<T> implements Comparator<List<T>> {
    private final Comparator<T> elementComparator;

    /** @param elementComparator element comparator */
    public ListComparator(Comparator<T> elementComparator) {
        Objects.requireNonNull(elementComparator);
        this.elementComparator = elementComparator;
    }

    @Override
    public int compare(List<T> l1, List<T> l2) {
        int size = Math.min(l1.size(), l2.size());
        T o1, o2;
        int c;
        for (int i = 0; i < size; i++) {
            o1 = l1.get(i);
            o2 = l2.get(i);
            c = elementComparator.compare(o1, o2);
            if (c != 0)
                if (c < 0)
                    return -1;
                else
                    return 1;
        }
        return Integer.compare(l1.size(), l2.size());
    }
}
