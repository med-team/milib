/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by dbolotin on 24.01.14.
 */
public final class GlobalObjectMappers {
    private GlobalObjectMappers() {
    }

    private static final Object sync = new Object();

    private static ObjectMapper ONE_LINE = null;
    private static ObjectMapper ONE_LINE_ORDERED = null;
    private static ObjectMapper PRETTY = null;

    public static String toOneLine(Object object) throws JsonProcessingException {
        String str = GlobalObjectMappers.getOneLine().writeValueAsString(object);

        if (str.contains("\n"))
            throw new RuntimeException("Internal error.");

        return str;
    }

    private static final List<Consumer<ObjectMapper>> mapperModifiers = new ArrayList<>();

    public static void addModifier(Consumer<ObjectMapper> modifier) {
        synchronized (sync) {
            mapperModifiers.add(modifier);
            ONE_LINE = null;
            PRETTY = null;
        }
    }

    public static ObjectMapper getOneLine() {
        synchronized (sync) {
            if (ONE_LINE == null) {
                ONE_LINE = new ObjectMapper()
                        .configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false)
                        .enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
                for (Consumer<ObjectMapper> modifier : mapperModifiers)
                    modifier.accept(ONE_LINE);
            }
            return ONE_LINE;
        }
    }

    public static ObjectMapper getOneLineOrdered() {
        synchronized (sync) {
            if (ONE_LINE_ORDERED == null) {
                ONE_LINE_ORDERED = JsonMapper.builder()
                        .enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                        .build()
                        .configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false)
                        .enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
                for (Consumer<ObjectMapper> modifier : mapperModifiers)
                    modifier.accept(ONE_LINE_ORDERED);
            }
            return ONE_LINE_ORDERED;
        }
    }

    public static ObjectMapper getPretty() {
        synchronized (sync) {
            if (PRETTY == null) {
                PRETTY = new ObjectMapper()
                        .enable(SerializationFeature.INDENT_OUTPUT)
                        .configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false)
                        .setDefaultPrettyPrinter(new DefaultPrettyPrinter1())
                        .enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
                for (Consumer<ObjectMapper> modifier : mapperModifiers)
                    modifier.accept(PRETTY);
            }
            return PRETTY;
        }
    }

    public static final class DefaultPrettyPrinter1 extends DefaultPrettyPrinter {
        public DefaultPrettyPrinter1() {
        }

        public DefaultPrettyPrinter1(DefaultPrettyPrinter base) {
            super(base);
        }

        @Override
        public void writeObjectFieldValueSeparator(JsonGenerator jg) throws IOException {
            jg.writeRaw(": ");
        }

        @Override
        public DefaultPrettyPrinter createInstance() {
            return new DefaultPrettyPrinter1(this);
        }
    }
}
