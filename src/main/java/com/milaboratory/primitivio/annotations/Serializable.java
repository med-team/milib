/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.primitivio.annotations;

import com.milaboratory.primitivio.ObjectMapperProvider;
import com.milaboratory.primitivio.Serializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Serializable {
    /** Set serializer for this class */
    Class<? extends Serializer> by() default Serializer.class;

    /** Use for polymorphic serialization */
    CustomSerializer[] custom() default {};

    /**
     * If true, Jackson will be used to covert object to string, and resulting string will be serialized, the opposite
     * operation will be used for deserialization.
     */
    boolean asJson() default false;

    /** Allows to customise object mapper that will be used for serialization if {@link #asJson()} option is true. */
    Class<? extends ObjectMapperProvider> objectMapperBy() default ObjectMapperProvider.Default.class;
}
