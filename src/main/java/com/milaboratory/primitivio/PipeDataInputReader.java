/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.primitivio;

import cc.redberry.pipe.OutputPortCloseable;

public final class PipeDataInputReader<O> implements OutputPortCloseable<O> {
    private final Class<O> oClass;
    private final PrimitivI input;
    private long count;

    public PipeDataInputReader(Class<O> oClass, PrimitivI input) {
        this(oClass, input, Long.MAX_VALUE);
    }

    public PipeDataInputReader(Class<O> oClass, PrimitivI input, long count) {
        this.oClass = oClass;
        this.input = input;
        this.count = count;
    }

    @Override
    public void close() {
        input.close();
    }

    @Override
    public synchronized O take() {
        if (count <= 0)
            return null;
        O obj = input.readObject(oClass);
        --count;
        return obj;
    }
}
