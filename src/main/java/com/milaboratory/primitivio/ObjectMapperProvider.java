package com.milaboratory.primitivio;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.milaboratory.primitivio.annotations.Serializable;
import com.milaboratory.util.GlobalObjectMappers;

/**
 * By implementing this interface, one can customise object mapper that will be used for serialization in classes with
 * activated {@link Serializable#asJson()} option.
 */
public abstract class ObjectMapperProvider {
    private final ObjectMapper objectMapper;

    protected ObjectMapperProvider(ObjectMapper objectMapper) {
        if (!objectMapper.isEnabled(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)) {
            throw new IllegalArgumentException("objectMapper must support SORT_PROPERTIES_ALPHABETICALLY");
        }
        this.objectMapper = objectMapper;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static final class Default extends ObjectMapperProvider {
        public Default() {
            super(GlobalObjectMappers.getOneLineOrdered());
        }
    }
}
