/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.io.sequence;

import com.milaboratory.core.sequence.quality.FunctionWithIndex;

import java.util.Iterator;
import java.util.function.Function;

public class SequenceReadWrapper<P> implements SequenceRead {
    final SequenceRead read;
    final P payload;

    public SequenceReadWrapper(SequenceRead read, P payload) {
        this.read = read;
        this.payload = payload;
    }

    public SequenceRead unwrap() {
        return read;
    }

    public P getPayload() {
        return payload;
    }

    @Override
    public int numberOfReads() {
        return read.numberOfReads();
    }

    @Override
    public SingleRead getRead(int i) {
        return read.getRead(i);
    }

    @Override
    public long getId() {
        return read.getId();
    }

    @Override
    public Iterator<SingleRead> iterator() {
        return read.iterator();
    }

    @Override
    public SequenceReadWrapper<P> mapReads(Function<SingleRead, SingleRead> mapping) {
        return new SequenceReadWrapper<P>(read.mapReads(mapping), payload);
    }

    @Override
    public SequenceReadWrapper<P> mapReadsWithIndex(FunctionWithIndex<SingleRead, SingleRead> mapping) {
        return new SequenceReadWrapper<>(read.mapReadsWithIndex(mapping), payload);
    }
}
