/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.io.sequence.fasta;

import com.milaboratory.core.io.sequence.SingleRead;
import com.milaboratory.core.io.sequence.SingleSequenceWriter;
import com.milaboratory.core.sequence.NucleotideSequence;

import java.io.FileNotFoundException;

/**
 * @author Dmitry Bolotin
 * @author Stanislav Poslavsky
 */
public final class FastaSequenceWriterWrapper implements SingleSequenceWriter {
    final FastaWriter<NucleotideSequence> sequenceFastaWriter;

    public FastaSequenceWriterWrapper(String fileName) throws FileNotFoundException {
        sequenceFastaWriter = new FastaWriter<>(fileName);
    }

    @Override
    public void write(SingleRead read) {
        sequenceFastaWriter.write(read.getDescription(), read.getData().getSequence());
    }

    @Override
    public void flush() {
        sequenceFastaWriter.flush();
    }

    @Override
    public void close() {
        sequenceFastaWriter.close();
    }
}
