/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.mutations;

import com.milaboratory.core.Range;
import com.milaboratory.core.alignment.Alignment;

/**
 * @author Dmitry Bolotin
 * @author Stanislav Poslavsky
 */
public class MutationConsensusBuilder {
    final CoverageCounter coverage;
    final MutationsCounter mutations;

    public MutationConsensusBuilder(Range seqRange) {
        this.coverage = new CoverageCounter(seqRange);
        this.mutations = new MutationsCounter();
    }

    public void aggregate(Alignment<?> alignment, CoverageCounter.Provider coverageFunction) {
        coverage.aggregate(alignment.getSequence1Range(), coverageFunction);
        MutationsEnumerator enumerator = new MutationsEnumerator(alignment.getAbsoluteMutations());
        while (enumerator.next()) {
            mutations.adjust(alignment.getAbsoluteMutations(),
                    enumerator,
                    (int) coverageFunction.delta(Mutation.getPosition(enumerator.getOffset())));
        }
    }
}
