/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.mutations;

/**
 * @author Dmitry Bolotin
 * @author Stanislav Poslavsky
 */
public final class MutationsEnumerator {
    private final int[] mutations;
    private int offset = 0;
    private int length = 0;
    private MutationType type;

    public MutationsEnumerator(Mutations mutations) {
        this.mutations = mutations.mutations;
    }

    public int getOffset() {
        return offset;
    }

    public int getLength() {
        return length;
    }

    public MutationType getType() {
        return type;
    }

    public boolean next() {
        if (offset + length >= mutations.length)
            return false;
        offset += length;
        length = 1;
        type = Mutation.getType(mutations[offset]);
        if (type == MutationType.Insertion) {
            MutationType t;
            while (offset + length < mutations.length) {
                t = Mutation.getType(mutations[offset + length]);
                if (t == type && Mutation.getPosition(mutations[offset])
                        == Mutation.getPosition(mutations[offset + length]))
                    ++length;
                else break;
            }
        }
        return true;
    }
}
