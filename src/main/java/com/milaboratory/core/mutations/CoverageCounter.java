/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.mutations;

import com.milaboratory.core.Range;

/**
 * @author Dmitry Bolotin
 * @author Stanislav Poslavsky
 */
public final class CoverageCounter {
    final int refFrom, refTo;
    final long[] counters;

    public CoverageCounter(Range seqRange) {
        this(seqRange.getFrom(), seqRange.getTo());
    }

    public CoverageCounter(int refFrom, int refTo) {
        this.refFrom = refFrom;
        this.refTo = refTo;
        this.counters = new long[refTo - refFrom];
    }

    public void aggregate(final Range r, final Provider provider) {
        final int from = r.getFrom(), to = r.getTo();
        if (from < refFrom || to > refTo)
            throw new IndexOutOfBoundsException();
        for (int i = from; i < to; ++i)
            counters[i] += provider.delta(i);
    }

    public void aggregate(final Range r, final int delta) {
        aggregate(r, constantDelta(delta));
    }

    public long count(int position) {
        return counters[position];
    }

    public interface Provider {
        long delta(int position);
    }

    public static Provider constantDelta(final int delta) {
        return new Provider() {
            @Override
            public long delta(int position) {
                return delta;
            }
        };
    }
}
