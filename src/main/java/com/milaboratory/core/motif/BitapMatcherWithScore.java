/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.motif;

public interface BitapMatcherWithScore extends BitapMatcher {
    /**
     * Match scoring in bits. 2^(-score) roughly approximates the probability of random match (i.e. E-value without
     * corrections for the size of the target sequence, and number of sequences), taking
     * the number of mismatches, wildcard matches etc. into account.
     */
    double getBitScore();

    /** maximal possible bit score minus actual bit score */
    double getBitScoreCost();
}
