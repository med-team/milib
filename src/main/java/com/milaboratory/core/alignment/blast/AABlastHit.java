/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.blast;

import com.milaboratory.core.Range;
import com.milaboratory.core.alignment.Alignment;
import com.milaboratory.core.sequence.AminoAcidSequence;

public class AABlastHit<P> extends BlastHit<AminoAcidSequence, P> {
    public AABlastHit(Alignment<AminoAcidSequence> alignment, P recordPayload, BlastHit<AminoAcidSequence, ?> hit) {
        super(alignment, recordPayload, hit);
    }

    public AABlastHit(Alignment<AminoAcidSequence> alignment, P recordPayload, double score, double bitScore, double eValue, Range subjectRange, String subjectId, String subjectTitle) {
        super(alignment, recordPayload, score, bitScore, eValue, subjectRange, subjectId, subjectTitle);
    }
}
