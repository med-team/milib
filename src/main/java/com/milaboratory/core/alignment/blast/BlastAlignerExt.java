/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.blast;

import com.milaboratory.core.Range;
import com.milaboratory.core.alignment.Alignment;
import com.milaboratory.core.sequence.Sequence;

public class BlastAlignerExt<S extends Sequence<S>> extends BlastAlignerExtAbstract<S, BlastHitExt<S>> {
    public BlastAlignerExt(BlastDB database) {
        super(database);
    }

    public BlastAlignerExt(BlastDB database, BlastAlignerParameters parameters) {
        super(database, parameters);
    }

    @Override
    protected BlastHitExt<S> createHit(Alignment alignment, double score, double bitScore, double eValue, Range subjectRange, String subjectId, String subjectTitle) {
        return new BlastHitExt<>(alignment, score, bitScore, eValue, subjectRange, subjectId, subjectTitle);
    }
}
