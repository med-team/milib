/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.blast;

import com.milaboratory.core.sequence.Alphabet;
import com.milaboratory.core.sequence.AminoAcidSequence;
import com.milaboratory.core.sequence.NucleotideSequence;

public enum BlastTask {
    BlastN(NucleotideSequence.ALPHABET, "blastn"),
    BlastNShort(NucleotideSequence.ALPHABET, "blastn-short"),
    MegaBlast(NucleotideSequence.ALPHABET, "megablast"),
    DCMegablast(NucleotideSequence.ALPHABET, "dc-megablast"),
    RMBlastN(NucleotideSequence.ALPHABET, "rmblastn"),
    BlastP(AminoAcidSequence.ALPHABET, "blastp"),
    BlastPShort(AminoAcidSequence.ALPHABET, "blastp-fast"),
    BlastPFast(AminoAcidSequence.ALPHABET, "blastp-short");

    final Alphabet<?> alphabet;
    final String value;

    BlastTask(Alphabet<?> alphabet, String value) {
        this.alphabet = alphabet;
        this.value = value;
    }
}
