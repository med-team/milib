/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.benchmark;

import cc.redberry.pipe.CUtils;
import cc.redberry.pipe.OutputPort;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.milaboratory.core.sequence.NucleotideSequence;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE)
public final class Challenge {
    final NucleotideSequence[] db;
    @JsonIgnore
    public final ChallengeParameters parameters;
    public final List<KAlignerQuery> queries;
    public final long seed;

    public Challenge(NucleotideSequence[] db, List<KAlignerQuery> queries,
                     ChallengeParameters parameters, long seed) {
        this.db = db;
        this.parameters = parameters;
        this.queries = queries;
        this.seed = seed;
    }

    public NucleotideSequence[] getDB() {
        return db;
    }

    public OutputPort<KAlignerQuery> queries() {
        return CUtils.asOutputPort(queries);
    }
}
