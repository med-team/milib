/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.benchmark;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.milaboratory.core.Range;
import com.milaboratory.core.alignment.Alignment;
import com.milaboratory.core.mutations.Mutations;
import com.milaboratory.core.sequence.NucleotideSequence;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE)
public class KAlignerQuery {
    public final int targetId;
    public final List<Range> queryClusters, targetClusters;
    public final List<Mutations<NucleotideSequence>> mutationsInTarget;
    public final NucleotideSequence query;
    public final Alignment<NucleotideSequence> expectedAlignment;

    public KAlignerQuery(int targetId, List<Range> queryClusters, List<Range> targetClusters,
                         List<Mutations<NucleotideSequence>> mutationsInTarget, NucleotideSequence query,
                         Alignment<NucleotideSequence> expectedAlignment) {
        this.targetId = targetId;
        this.queryClusters = queryClusters;
        this.targetClusters = targetClusters;
        this.mutationsInTarget = mutationsInTarget;
        this.query = query;
        this.expectedAlignment = expectedAlignment;
    }

    public KAlignerQuery(NucleotideSequence sequence) {
        this(-1, null, null, null, sequence, null);
    }

    public boolean isFalse(){
        return targetId == -1;
    }

    @Override
    public String toString() {
        return "Challenge{" +
                "targetId=" + targetId +
                ", queryClusters=" + queryClusters +
                ", targetClusters=" + targetClusters +
                ", mutationsInTarget=" + mutationsInTarget +
                ", query=" + query +
                '}';
    }
}
