/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import com.milaboratory.core.sequence.Sequence;

/**
 * Represents aligner that can align a sequence against a set of other sequences.
 *
 * @param <S> sequence type
 * @author Dmitry Bolotin
 * @author Stanislav Poslavsky
 */
public interface BatchAligner<S extends Sequence<S>, H extends AlignmentHit<S, ?>> {
    AlignmentResult<H> align(S sequence);

    AlignmentResult<H> align(S sequence, int from, int to);
}
