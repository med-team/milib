/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import java.util.List;

public class PipedAlignmentResultImpl<H extends AlignmentHit<?, ?>, Q>
        extends AlignmentResultImpl<H>
        implements PipedAlignmentResult<H, Q> {
    final Q query;

    public PipedAlignmentResultImpl(List<H> alignmentHits, Q query) {
        super(alignmentHits);
        this.query = query;
    }

    @Override
    public Q getQuery() {
        return query;
    }

    @Override
    public String toString() {
        return query + " -> " + super.toString();
    }
}
