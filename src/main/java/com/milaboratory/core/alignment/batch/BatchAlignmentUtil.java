/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import com.milaboratory.core.sequence.Sequence;

import java.util.Comparator;

public class BatchAlignmentUtil {
    public static final Comparator<AlignmentHit<?, ?>> ALIGNMENT_SCORE_HIT_COMPARATOR = new Comparator<AlignmentHit<?, ?>>() {
        @Override
        public int compare(AlignmentHit<?, ?> o1, AlignmentHit<?, ?> o2) {
            return Float.compare(o2.getAlignment().getScore(), o1.getAlignment().getScore());
        }
    };

    public static final SequenceExtractor DUMMY_EXTRACTOR = new SequenceExtractor() {
        @Override
        public Sequence extract(Object object) {
            return ((HasSequence) object).getSequence();
        }
    };
}
