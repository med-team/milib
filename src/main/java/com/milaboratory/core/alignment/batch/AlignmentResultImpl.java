/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import java.util.Collections;
import java.util.List;

public class AlignmentResultImpl<H extends AlignmentHit<?, ?>> implements AlignmentResult<H> {
    final List<H> hits;

    public AlignmentResultImpl() {
        this.hits = Collections.emptyList();
    }

    public AlignmentResultImpl(List<H> hits) {
        this.hits = hits;
    }

    @Override
    public List<H> getHits() {
        return hits;
    }

    @Override
    public final H getBestHit() {
        return hits.isEmpty() ? null : hits.get(0);
    }

    @Override
    public boolean hasHits() {
        return !hits.isEmpty();
    }

    @Override
    public String toString() {
        return !hasHits() ? "Empty result." : (hits.size() + " hits.");
    }
}
