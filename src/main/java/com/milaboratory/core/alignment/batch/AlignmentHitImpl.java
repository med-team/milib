/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import com.milaboratory.core.alignment.Alignment;
import com.milaboratory.core.sequence.Sequence;

public class AlignmentHitImpl<S extends Sequence<S>, P> implements AlignmentHit<S, P> {
    final Alignment<S> alignment;
    final P recordPayload;

    public AlignmentHitImpl(Alignment<S> alignment, P recordPayload) {
        this.alignment = alignment;
        this.recordPayload = recordPayload;
    }

    @Override
    public Alignment<S> getAlignment() {
        return alignment;
    }

    @Override
    public P getRecordPayload() {
        return recordPayload;
    }
}
