/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.batch;

import com.milaboratory.core.sequence.Sequence;

/**
 * Interface for aligners with self-managed database (user can directly add subject sequences before running alignment)
 *
 * @param <S> type of sequence
 * @param <P> type of record payload, used to store additional information along with sequence to simplify it's
 *            subsequent identification in result (e.g. {@link Integer} to just index sequences.
 */
public interface WithBase<S extends Sequence<S>, P> {
    /**
     * Adds a record to the base of this aligner (a set of subject sequences that this instance aligns queries
     * with).
     *
     * @param sequence sequence
     * @param payload  payload to store additional information with this record (can be retrieved from resulting {@link
     *                 AlignmentHit})
     */
    void addReference(S sequence, P payload);
}
