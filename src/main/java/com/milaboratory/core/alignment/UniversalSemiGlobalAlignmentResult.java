/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment;

import java.util.Objects;

public final class UniversalSemiGlobalAlignmentResult {
    public final int score;
    public final int leftSkipped1,
            rightSkipped1,
            leftSkipped2,
            rightSkipped2;

    public UniversalSemiGlobalAlignmentResult(int score,
                                              int leftSkipped1, int rightSkipped1, int leftSkipped2, int rightSkipped2) {
        this.score = score;
        this.leftSkipped1 = leftSkipped1;
        this.rightSkipped1 = rightSkipped1;
        this.leftSkipped2 = leftSkipped2;
        this.rightSkipped2 = rightSkipped2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniversalSemiGlobalAlignmentResult)) return false;
        UniversalSemiGlobalAlignmentResult that = (UniversalSemiGlobalAlignmentResult) o;
        return score == that.score && leftSkipped1 == that.leftSkipped1 && rightSkipped1 == that.rightSkipped1 && leftSkipped2 == that.leftSkipped2 && rightSkipped2 == that.rightSkipped2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(score, leftSkipped1, rightSkipped1, leftSkipped2, rightSkipped2);
    }
}
