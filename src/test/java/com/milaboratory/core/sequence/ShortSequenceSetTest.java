package com.milaboratory.core.sequence;

import com.milaboratory.test.TestUtil;
import com.milaboratory.util.FormatUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;

import static com.milaboratory.core.sequence.ShortSequenceSet.fromLong;
import static com.milaboratory.core.sequence.ShortSequenceSet.toLong;

public class ShortSequenceSetTest {
    @Test
    public void test1() throws IOException {
        HashSet<NucleotideSequence> hashSet = new HashSet<>();
        ShortSequenceSet shortSet = new ShortSequenceSet();
        int nucleotides = 0;
        for (int i = 0; i < 100000; i++) {
            NucleotideSequence seq = TestUtil.randomSequence(NucleotideSequence.ALPHABET, 10, 20);
            nucleotides += seq.size();
            hashSet.add(seq);
            shortSet.add(seq);
            Assert.assertEquals(seq, fromLong(toLong(seq)));
        }
        Assert.assertEquals(hashSet, new HashSet<>(shortSet));
        byte[] bytes = shortSet.serialize();
        System.out.println("" + hashSet.size() + " elements with " + FormatUtils.bytesToString(nucleotides / 4) +
                " in raw nucleotide entropy serialized into " + FormatUtils.bytesToString(bytes.length));
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        Assert.assertTrue(ShortSequenceSet.isShortSequenceSet(is));
        ShortSequenceSet deserialized = ShortSequenceSet.deserialize(is);
        Assert.assertEquals(shortSet, deserialized);
    }
}