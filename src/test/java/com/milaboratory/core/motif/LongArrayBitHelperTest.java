package com.milaboratory.core.motif;

import org.junit.Assert;
import org.junit.Test;

public class LongArrayBitHelperTest {
    public static long[] a1() {
        return new long[]{0xF0000000000F0000L, 0xFF000000F0000000L, 0x000F00000000000FL};
    }

    @Test
    public void test1() {
        long[] a;

        a = a1();
        LongArrayBitHelper.leftShiftLittleEndian(a, 4);
        //                                 {0xF0000000000F0000L, 0xFF000000F0000000L, 0x000F00000000000FL};
        Assert.assertArrayEquals(new long[]{0x0000000000F00000L, 0xF000000F0000000FL, 0x00F00000000000FFL}, a);

        a = a1();
        LongArrayBitHelper.leftShiftBigEndian(a, 4);
        //                                 {0xF0000000000F0000L, 0xFF000000F0000000L, 0x000F00000000000FL};
        Assert.assertArrayEquals(new long[]{0x0000000000F0000FL, 0xF000000F00000000L, 0x00F00000000000F0L}, a);

        a = a1();
        LongArrayBitHelper.rightShiftLittleEndian(a, 4);
        //                                 {0xF0000000000F0000L, 0xFF000000F0000000L, 0x000F00000000000FL};
        Assert.assertArrayEquals(new long[]{0x0F0000000000F000L, 0xFFF000000F000000L, 0x0000F00000000000L}, a);

        a = a1();
        LongArrayBitHelper.rightShiftBigEndian(a, 4);
        //                                 {0xF0000000000F0000L, 0xFF000000F0000000L, 0x000F00000000000FL};
        Assert.assertArrayEquals(new long[]{0x0F0000000000F000L, 0x0FF000000F000000L, 0x0000F00000000000L}, a);
    }
}