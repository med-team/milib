/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.blast;

import org.junit.Assume;
import org.junit.Before;

import java.util.concurrent.atomic.AtomicBoolean;

public class BlastTest {
    static final AtomicBoolean first = new AtomicBoolean(true);

    @Before
    public void setUp() {
        Assume.assumeTrue(Blast.isBlastAvailable());
        if (first.compareAndSet(true, false))
            System.out.println("Blast: " + Blast.getBlastCommand(Blast.CMD_BLASTN, false));
    }
}
