/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment.blast;

import com.milaboratory.core.sequence.NucleotideSequence;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BlastDBBuilderTest extends BlastTest {
    @Test
    public void test1() throws Exception {
        List<NucleotideSequence> seqs = new ArrayList<>();
        seqs.add(new NucleotideSequence("ATTAGACACAGACACA"));
        seqs.add(new NucleotideSequence("GATACACCACCGATGCTGGAGATGCATGCTAGCGGCGCGGATAGCTGCATG"));
        long bases = 0;
        for (NucleotideSequence seq : seqs)
            bases += seq.size();

        BlastDB db = BlastDBBuilder.build(seqs, true);

        assertEquals(NucleotideSequence.ALPHABET, db.getAlphabet());
        assertEquals(bases, db.getLettersCount());
        assertEquals(seqs.size(), db.getRecordsCount());

        assertEquals(seqs.get(1), db.retriveSequenceById(BlastDBBuilder.getIdFasta(1)));
    }
}