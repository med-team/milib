/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import org.apache.commons.math3.random.Well19937c;
import org.junit.Assert;
import org.junit.Test;

public class BitArrayTest {
    @Test
    public void test1() {
        int its = 10000;
        Well19937c r = RandomUtil.getThreadLocalRandom();
        for (int i = 0; i < its; i++) {
            int len = 1 + r.nextInt(100);
            BitArray ba = new BitArray(len);
            for (int j = 0; j < len; j++)
                ba.set(j, r.nextBoolean());
            int k = r.nextInt(len);
            BitArray ba1 = new BitArray(len);
            ba1.set(k);
            BitArray ba2 = ba.clone();
            ba2.xor(ba1);
            BitArray ba3 = ba.clone();
            ba3.not();
            BitArray ba4 = ba2.clone();
            ba4.not();
            BitArray ba5 = ba3.clone();
            ba5.not();

            ba3.xor(ba4);

            Assert.assertEquals(ba1, ba3);
            Assert.assertEquals(ba, ba5);
        }
    }

    @Test
    public void nextBit1() {
        BitArray ba = new BitArray(100);
        ba.set(1);
        ba.set(2);
        ba.set(3);
        ba.set(8);
        ba.set(12);
        ba.set(35);
        ba.set(74);
        Assert.assertEquals(1, ba.nextBit(0));
        Assert.assertEquals(1, ba.nextBit(1));
        Assert.assertEquals(2, ba.nextBit(2));
        Assert.assertEquals(3, ba.nextBit(3));
        Assert.assertEquals(8, ba.nextBit(4));
        Assert.assertEquals(8, ba.nextBit(8));
        Assert.assertEquals(12, ba.nextBit(9));
        Assert.assertEquals(12, ba.nextBit(12));
        Assert.assertEquals(35, ba.nextBit(13));
        Assert.assertEquals(35, ba.nextBit(35));
        Assert.assertEquals(74, ba.nextBit(36));
        Assert.assertEquals(-1, ba.nextBit(75));
    }

    @Test
    public void nextBit2() {
        BitArray ba = new BitArray(100);
        ba.set(99);
        Assert.assertEquals(99, ba.nextBit(36));
        Assert.assertEquals(99, ba.nextBit(99));
        Assert.assertEquals(-1, ba.nextBit(100));
    }

    @Test
    public void nextBit3() {
        BitArray ba = new BitArray(101);
        ba.set(100);
        Assert.assertEquals(100, ba.nextBit(36));
        Assert.assertEquals(100, ba.nextBit(100));
        Assert.assertEquals(-1, ba.nextBit(101));
    }
}
