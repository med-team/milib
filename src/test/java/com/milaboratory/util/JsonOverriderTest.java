/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.milaboratory.test.TestUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

public class JsonOverriderTest {
    @JsonAutoDetect(fieldVisibility = ANY, isGetterVisibility = NONE, getterVisibility = NONE)
    public static final class A {
        int a;
        int[] b;

        public A() {
        }

        public A(int a, int... b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            A a1 = (A) o;
            return a == a1.a && Arrays.equals(b, a1.b);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(a);
            result = 31 * result + Arrays.hashCode(b);
            return result;
        }
    }

    @JsonAutoDetect(fieldVisibility = ANY, isGetterVisibility = NONE, getterVisibility = NONE)
    public static final class B {
        A a;
        A aa;
        String s;

        A[] aArray;

        public B() {
        }

        public B(A a, A aa, String s, A[] aArray) {
            this.a = a;
            this.aa = aa;
            this.s = s;
            this.aArray = aArray;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            B b = (B) o;
            return Objects.equals(a, b.a) && Objects.equals(aa, b.aa) && Objects.equals(s, b.s);
        }

        @Override
        public int hashCode() {
            return Objects.hash(a, aa, s);
        }
    }

    @Test
    public void test1() {
        B b = new B(new A(1, 2, 3), new A(7), "Hi!", new A[]{new A(10)});
        TestUtil.assertJson(b);
        B b1 = JsonOverrider.override(b, B.class, "a.a=12");
        Assert.assertEquals(12, b1.a.a);
        B b2 = JsonOverrider.override(b, B.class, "a.b=[]");
        Assert.assertEquals(0, b2.a.b.length);
        B b3 = JsonOverrider.override(b, B.class, "a.b=[1, 2, 3]");
        Assert.assertEquals(3, b3.a.b.length);
        B b4 = JsonOverrider.override(b, B.class, "a=null");
        Assert.assertNull(b4.a);
        B b5 = JsonOverrider.override(b, B.class, "aArray[0].a=13");
        Assert.assertEquals(13, b5.aArray[0].a);
    }

    @Test
    public void test2() {
        B b = new B(new A(1, 2, 3), new A(7), "Hi!", new A[0]);
        // No error for unknown field
        JsonOverrider.override(b, B.class, "a.c=12");
    }
}
