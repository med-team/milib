/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.util;

import com.milaboratory.test.TestUtil;
import org.apache.commons.math3.random.Well19937c;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TopNTest {
    @Test
    public void test1() {
        Well19937c r = RandomUtil.getThreadLocalRandom();
        int its = TestUtil.its(100, 10000);
        for (int i = 0; i < its; i++) {
            int n = r.nextInt(10);
            int inputLen = r.nextInt(100);
            int[] data = new int[inputLen];
            TopN<Integer> t = new TopN<>(n);
            for (int j = 0; j < inputLen; j++)
                t.add(data[j] = r.nextInt(200));
            Arrays.sort(data);
            List<Integer> result = t.result();
            int k = Math.min(n, inputLen);
            List<Integer> expected = new ArrayList<>();
            for (int i1 : Arrays.copyOfRange(data, data.length - k, data.length))
                expected.add(i1);
            Assert.assertEquals(expected, result);
        }
    }
}
