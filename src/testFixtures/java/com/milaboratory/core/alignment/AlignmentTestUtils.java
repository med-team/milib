/*
 * Copyright (c) 2022 MiLaboratories Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.milaboratory.core.alignment;

import com.milaboratory.core.sequence.Sequence;
import org.junit.Assert;

public class AlignmentTestUtils {
    public static <S extends Sequence<S>> void assertAlignment(Alignment<S> alignment, S s2) {
        Assert.assertEquals(
                alignment.getRelativeMutations().mutate(alignment.sequence1.getRange(
                        alignment.getSequence1Range())), s2.getRange(alignment.getSequence2Range()));
    }

    public static <S extends Sequence<S>> void assertAlignment(Alignment<S> alignment, S s2, AlignmentScoring<S> scoring) {
        assertAlignment(alignment, s2);
        int calculatedScoring = alignment.calculateScore(scoring);
        if (calculatedScoring != alignment.getScore()) {
            System.out.println("Actual score: " + alignment.getScore());
            System.out.println("Expected score: " + calculatedScoring);
            System.out.println("Actual alignment: ");
            System.out.println(alignment);
            System.out.println();
        }
        Assert.assertEquals(calculatedScoring, alignment.getScore(), 0.1);
    }
}
