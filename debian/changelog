milib (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.0+dfsg
  * Refreshing patches
  * Raising Standards version to 4.6.2 (no change)

 -- Pierre Gruet <pgt@debian.org>  Fri, 30 Dec 2022 14:38:35 +0100

milib (2.1.0+dfsg-2) unstable; urgency=medium

  * Revising the list of (build-)dependencies and the classpath

 -- Pierre Gruet <pgt@debian.org>  Sun, 27 Nov 2022 14:58:01 +0100

milib (2.1.0+dfsg-1) unstable; urgency=medium

  [ Pierre Gruet ]
  * New upstream version 2.1.0+dfsg
  * Refreshing patches
  * Refreshing d/copyright
  * Trim trailing whitespace.
  * Patching out a flaky assertion in a test

  [ Andreas Tille ]
  * d/watch: Create proper upstream filename

 -- Pierre Gruet <pgt@debian.org>  Mon, 14 Nov 2022 21:57:47 +0100

milib (2.0.0+dfsg-2) unstable; urgency=medium

  * Avoiding installing the same jar in /usr/share/java and in the maven-repo
    tree, using symlinks instead
  * Setting myself as Uploader

 -- Pierre Gruet <pgt@debian.org>  Fri, 08 Jul 2022 09:03:18 +0200

milib (2.0.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

  [ Steffen Moeller ]
  * d/u/metadata: Updates and cosmetics

  [ Andreas Tille ]
  * Standards-Version: 4.6.1 (routine-update)
  * Removing a patch as Maven is not used anymore

  [ Pierre Gruet ]
  * Removing files from debian/ we needed for Maven previously
  * Repacking without the gradle jar
  * Refreshing build-dependencies
  * Refreshing Maven rules
  * Patching build.gradle.kts to form a gradle build file
  * Adding the version number of the package to our build.gradle file
  * Build-depending on javahelper and maven-repo-helper, determining
    dependencies with javahelper
  * Defining the classpath of the built jar in d/libmilib-java.classpath
  * Patching code:
    - Changing a call to some guava method
    - Deactivating a test relying on a build properties file we don't provide

 -- Pierre Gruet <pgt@debian.org>  Sun, 03 Jul 2022 13:48:39 +0200

milib (1.13-1) unstable; urgency=medium

  * Team upload

  [ Steffen Moeller ]
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * watch file standard 4 (routine-update)
  * DEP3

 -- Andreas Tille <tille@debian.org>  Tue, 19 Jan 2021 10:04:08 +0100

milib (1.12-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Steffen Moeller <moeller@debian.org>  Thu, 09 Jan 2020 14:00:32 +0100

milib (1.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 14:43:19 +0200

milib (1.10-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against fixed libredberry-pipe-java to get dependency information
    correct.

 -- Andreas Tille <tille@debian.org>  Thu, 21 Feb 2019 10:25:29 +0100

milib (1.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Build-Depends: libpicocli-java
  * Rebuild
    Closes: #920839
  * Take over package to Debian Med team

 -- Andreas Tille <tille@debian.org>  Tue, 12 Feb 2019 14:22:59 +0100

milib (1.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Versioned Build-Depends: libredberry-pipe-java (>= 1.0.0~alpha0~)
  * Build-Depends: libjaxb-java
  * Use jaxb-api

 -- Andreas Tille <tille@debian.org>  Mon, 28 Jan 2019 10:39:55 +0100

milib (1.8.4-1) unstable; urgency=medium

  * Initial release (Closes: #903134).
    Reupload with build dependencies accepted in distribution.

 -- Steffen Moeller <moeller@debian.org>  Wed, 09 Jan 2019 17:52:14 +0100
